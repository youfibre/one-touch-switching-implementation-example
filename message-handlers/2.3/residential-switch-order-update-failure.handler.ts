import { OTSClient } from '@youfibre/otsc';
import { RepositoryBundle } from '../../data/repository-bundle.type';
import { ResidentialSwitchOrderUpdateFailure } from '@youfibre/otsc';

export default function handleResidentialSwitchOrderUpdateFailure(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchOrderUpdateFailure
) {
  console.log('\n\n🚨 Received residentialSwitchOrderUpdateFailure: manual intervention required.');
  console.log(`🚨 Fault Code: ${body.residentialSwitchOrderUpdateFailure.faultCode}`);
  console.log(`🚨 Fault Reason: ${body.residentialSwitchOrderUpdateFailure.faultText}`);
  console.log(`🚨 Fault Element: ${body.residentialSwitchOrderUpdateFailure.faultElement || 'N/A'}`);
  console.log(`🚨 Fault Element Value: ${body.residentialSwitchOrderUpdateFailure.faultElementValue || 'N/A'}\n\n`);
}