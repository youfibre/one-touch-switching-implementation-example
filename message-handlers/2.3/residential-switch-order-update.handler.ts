import { OTSClient } from '@youfibre/otsc';
import { RepositoryBundle } from '../../data/repository-bundle.type';
import { ResidentialSwitchOrderUpdate } from '@youfibre/otsc';
import { ResidentialSwitchOrderUpdateConfirmation } from '@youfibre/otsc';
import { ResidentialSwitchOrderUpdateFailure } from '@youfibre/otsc';

async function switchOrderNotFound(client: OTSClient, rcpId: string, body: ResidentialSwitchOrderUpdate) {
  return client.sendMessage({
    envelope: {
      source: {
        type: 'RCPID',
        identity: rcpId,
        correlationID: body.envelope.destination.correlationID
      },
      destination: body.envelope.source
    },
  
    residentialSwitchOrderUpdateFailure: {
      faultCode: '301',
      faultText: 'Invalid or missing switch order reference',
      faultElement: 'switchOrderReference',
      faultElementValue: (body.residentialSwitchOrderUpdate.switchOrderReference || '').toString()
    }
  } as ResidentialSwitchOrderUpdateFailure);
}

export default function handleResidentialSwitchOrderUpdate(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchOrderUpdate
) {
  const switchOrder = repositories.switchOrderRepository.findByOrderReference(body.residentialSwitchOrderUpdate.switchOrderReference);
  if (!switchOrder) {
    console.error(`🚨 Failed to find Switch Order by Order Reference: ${body.residentialSwitchOrderUpdate.switchOrderReference}`);
    return switchOrderNotFound(client, rcpId, body);
  }

  switchOrder.switchDate = new Date(Date.parse(body.residentialSwitchOrderUpdate.plannedSwitchDate));

  return client.sendMessage({
    envelope: {
      source: body.envelope.destination,
      destination: body.envelope.source
    },
  
    residentialSwitchOrderUpdateConfirmation: {
      status: 'updated'
    }
  } as ResidentialSwitchOrderUpdateConfirmation);
}