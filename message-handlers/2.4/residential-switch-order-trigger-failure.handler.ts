import { OTSClient } from '@youfibre/otsc';
import { RepositoryBundle } from '../../data/repository-bundle.type';
import { ResidentialSwitchOrderTriggerFailure } from '@youfibre/otsc';

export default function handleResidentialSwitchOrderTriggerFailure(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchOrderTriggerFailure
) {
  console.log('\n\n🚨 Received residentialSwitchOrderTriggerFailure: manual intervention required.');
  console.log(`🚨 Fault Code: ${body.residentialSwitchOrderTriggerFailure.faultCode}`);
  console.log(`🚨 Fault Reason: ${body.residentialSwitchOrderTriggerFailure.faultText}`);
  console.log(`🚨 Fault Element: ${body.residentialSwitchOrderTriggerFailure.faultElement || 'N/A'}`);
  console.log(`🚨 Fault Element Value: ${body.residentialSwitchOrderTriggerFailure.faultElementValue || 'N/A'}\n\n`);
}