import { OTSClient } from '@youfibre/otsc';
import { RepositoryBundle } from '../../data/repository-bundle.type';
import { ResidentialSwitchOrderTrigger } from '@youfibre/otsc';
import { ResidentialSwitchOrderTriggerConfirmation } from '@youfibre/otsc';
import { ResidentialSwitchOrderTriggerFailure } from '@youfibre/otsc';
import { VoiceService } from '../../data/customer-repository.class';

async function switchOrderNotFound(client: OTSClient, rcpId: string, body: ResidentialSwitchOrderTrigger) {
  return client.sendMessage({
    envelope: {
      source: {
        type: 'RCPID',
        identity: rcpId,
        correlationID: body.envelope.destination.correlationID
      },
      destination: body.envelope.source
    },
  
    residentialSwitchOrderTriggerFailure: {
      faultCode: '401',
      faultText: 'Invalid or missing switch order reference',
      faultElement: 'switchOrderReference',
      faultElementValue: (body.residentialSwitchOrderTrigger.switchOrderReference || '').toString()
    }
  } as ResidentialSwitchOrderTriggerFailure);
}

export default function handleResidentialSwitchOrderTrigger(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchOrderTrigger
) {
  const switchOrder = repositories.switchOrderRepository.findByOrderReference(body.residentialSwitchOrderTrigger.switchOrderReference);
  if (!switchOrder) {
    console.error(`🚨 Failed to find Switch Order by Order Reference: ${body.residentialSwitchOrderTrigger.switchOrderReference}`);
    return switchOrderNotFound(client, rcpId, body);
  }

  switchOrder.currentStage = 'completed';

  // Once the switch order is completed, we send any final bills, terminate their services, etc.
  console.info(`\n\n📧\tSEND FINAL BILL TO CUSTOMER ${switchOrder.customer.name} <${switchOrder.customer.email}>`);
  console.info(`❌\tDeprovision customer services: ${switchOrder.customer.name} <${switchOrder.customer.email}>`);
  switchOrder.switchActions?.forEach((sa) => {
    if (!sa.switchActionData.service) return;

    if (sa.switchActionData.switchAction === 'port') {
      console.info(`📝\tPerform any porting-related activities for ${(sa.switchActionData.service as VoiceService).phoneNumber}`);
    }

    const serviceIndex = switchOrder.customer.services.indexOf(sa.switchActionData.service);
    if (serviceIndex > -1) {
      switchOrder.customer.services.splice(serviceIndex, 1);
    }
  })
  
  if (switchOrder.customer.services.length === 0) {
    repositories.customerRepository.remove(switchOrder.customer.accountNumber);
    console.info(`🗑️\tRemove customer from billing system: ${switchOrder.customer.name} <${switchOrder.customer.email}>`);
  }

  console.log('\n');

  return client.sendMessage({
    envelope: {
      source: body.envelope.destination,
      destination: body.envelope.source
    },
  
    residentialSwitchOrderTriggerConfirmation: {
      status: 'triggered'
    }
  } as ResidentialSwitchOrderTriggerConfirmation);
}