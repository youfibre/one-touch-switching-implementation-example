import { OTSClient } from '@youfibre/otsc';
import { RepositoryBundle } from '../../data/repository-bundle.type';
import { ResidentialSwitchOrderTriggerConfirmation } from '@youfibre/otsc';

export default function handleResidentialSwitchOrderTriggerConfirmation(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchOrderTriggerConfirmation
) {
  console.info(`\n\n✅ Residential Switch Order Trigger Confirmed\n\n`);
}