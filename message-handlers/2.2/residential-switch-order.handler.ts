import { OTSClient } from '@youfibre/otsc';
import { RepositoryBundle } from '../../data/repository-bundle.type';
import { ResidentialSwitchOrder } from '@youfibre/otsc';
import { ResidentialSwitchOrderConfirmation } from '@youfibre/otsc';
import { ResidentialSwitchOrderFailure } from '@youfibre/otsc';

async function switchOrderNotFound(client: OTSClient, rcpId: string, body: ResidentialSwitchOrder) {
  return client.sendMessage({
    envelope: {
      source: {
        type: 'RCPID',
        identity: rcpId,
        correlationID: body.envelope.destination.correlationID
      },
      destination: body.envelope.source
    },
  
    residentialSwitchOrderFailure: {
      faultCode: '201',
      faultText: 'Invalid or missing switch order reference',
      faultElement: 'switchOrderReference',
      faultElementValue: (body.residentialSwitchOrder.switchOrderReference || '').toString()
    }
  } as ResidentialSwitchOrderFailure);
}

async function accountNotRecognised(
  client: OTSClient,
  rcpId: string,
  body: ResidentialSwitchOrder,
  extra?: any
) {
  return client.sendMessage({
    envelope: {
      source: {
        type: 'RCPID',
        identity: rcpId,
        correlationID: body.envelope.destination.correlationID
      },
      destination: body.envelope.source
    },
  
    residentialSwitchOrderFailure: {
      faultCode: '206',
      faultText: 'Account not recognised',
      ...extra
    }
  } as ResidentialSwitchOrderFailure);
}

export default function handleResidentialSwitchOrder(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchOrder
) {
  const switchOrder = repositories.switchOrderRepository.findByOrderReference(body.residentialSwitchOrder.switchOrderReference);
  if (!switchOrder) {
    console.error(`🚨 Failed to find Switch Order by Order Reference: ${body.residentialSwitchOrder.switchOrderReference}`);
    return switchOrderNotFound(client, rcpId, body);
  }

  // Make sure that the services on the Switch Order haven't been used in another Switch Order which has become active.
  if (switchOrder.switchActions?.some(
    (sa) => sa.switchActionData.service && repositories.switchOrderRepository.hasActiveSwitchOrder(sa.switchActionData.service))
  ) {
    console.log(`Active Switch Order found for one or more services on Switch Order with Correlation ID ${switchOrder.correlationId} but notification format unknown to send to GP.`);
    return accountNotRecognised(client, rcpId, body, {
      faultElement: 'confusion',
      faultElementValue: 'Unsure of the format to inform GP that there is already an active Switch Order.'
    });
  }

  switchOrder.currentStage = 'order';
  switchOrder.switchDate = new Date(Date.parse(body.residentialSwitchOrder.plannedSwitchDate));

  return client.sendMessage({
    envelope: {
      source: body.envelope.destination,
      destination: body.envelope.source
    },
  
    residentialSwitchOrderConfirmation: {
      status: 'confirmed'
    }
  } as ResidentialSwitchOrderConfirmation);
}