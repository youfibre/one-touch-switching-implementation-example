import { RepositoryBundle } from '../../data/repository-bundle.type';
import { OTSClient } from '@youfibre/otsc';
import { ResidentialSwitchOrderConfirmation } from '@youfibre/otsc';
import { ResidentialSwitchOrderTrigger } from '@youfibre/otsc';

export default function handleResidentialSwitchOrderConfirmation(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchOrderConfirmation
) {
  console.info(`\n\n✅ Residential Switch Order Confirmed\n\n`);

  // Find the related Switch Order
  const switchOrder = repositories.switchOrderRepository.findByCorrelationId(body.envelope.destination.correlationID);
  if (!switchOrder) {
    console.error(`\n\n🚨 Could not locate related Switch Order by Correlation ID: ${body.envelope.destination.correlationID}\n\n`);
    return;
  }

  // @TODO: Receive KCI from supply chain. Once the services have
  //        been delivered, it is then permitted to send a "trigger",
  //        informing the LP that the old services may now be modified
  //        to meet the Switch Order (e.g. cease services).
  
  switchOrder.currentStage = 'completed';

  client.sendMessage({
    envelope: {
      source: body.envelope.destination,
      destination: body.envelope.source
    },

    residentialSwitchOrderTrigger: {
      switchOrderReference: switchOrder.switchOrderReference
    }
  } as ResidentialSwitchOrderTrigger);
}