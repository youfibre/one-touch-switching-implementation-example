import { OTSClient } from '@youfibre/otsc';
import { RepositoryBundle } from '../../data/repository-bundle.type';
import { ResidentialSwitchOrderCancellation } from '@youfibre/otsc';
import { ResidentialSwitchOrderCancellationConfirmation } from '@youfibre/otsc';
import { ResidentialSwitchOrderCancellationFailure } from '@youfibre/otsc';

async function switchOrderNotFound(client: OTSClient, rcpId: string, body: ResidentialSwitchOrderCancellation) {
  return client.sendMessage({
    envelope: {
      source: {
        type: 'RCPID',
        identity: rcpId,
        correlationID: body.envelope.destination.correlationID
      },
      destination: body.envelope.source
    },
  
    residentialSwitchOrderCancellationFailure: {
      faultCode: '501',
      faultText: 'Invalid or missing switch order reference',
      faultElement: 'switchOrderReference',
      faultElementValue: (body.residentialSwitchOrderCancellation.switchOrderReference || '').toString()
    }
  } as ResidentialSwitchOrderCancellationFailure);
}

export default function handleResidentialSwitchOrderCancellation(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchOrderCancellation
) {
  const switchOrder = repositories.switchOrderRepository.findByOrderReference(body.residentialSwitchOrderCancellation.switchOrderReference);
  if (!switchOrder) {
    console.error(`🚨 Failed to find Switch Order by Order Reference: ${body.residentialSwitchOrderCancellation.switchOrderReference}`);
    return switchOrderNotFound(client, rcpId, body);
  }

  switchOrder.currentStage = 'cancelled';

  return client.sendMessage({
    envelope: {
      source: body.envelope.destination,
      destination: body.envelope.source
    },
  
    residentialSwitchOrderCancellationConfirmation: {
      status: 'cancelled'
    }
  } as ResidentialSwitchOrderCancellationConfirmation);
}