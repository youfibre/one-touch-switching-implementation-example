import { RepositoryBundle } from '../../data/repository-bundle.type';
import { OTSClient } from '@youfibre/otsc';
import { ResidentialSwitchOrderCancellationFailure } from '@youfibre/otsc';

export default function handleResidentialSwitchOrderCancellationFailure(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchOrderCancellationFailure
) {
  console.log('\n\n🚨 Received residentialSwitchOrderCancellationFailure: manual intervention required.');
  console.log(`🚨 Fault Code: ${body.residentialSwitchOrderCancellationFailure.faultCode}`);
  console.log(`🚨 Fault Reason: ${body.residentialSwitchOrderCancellationFailure.faultText}`);
  console.log(`🚨 Fault Element: ${body.residentialSwitchOrderCancellationFailure.faultElement || 'N/A'}`);
  console.log(`🚨 Fault Element Value: ${body.residentialSwitchOrderCancellationFailure.faultElementValue || 'N/A'}\n\n`);
}