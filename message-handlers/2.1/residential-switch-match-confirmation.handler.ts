import { RepositoryBundle } from '../../data/repository-bundle.type';
import { OTSClient } from '@youfibre/otsc';
import { ResidentialSwitchMatchConfirmation } from '@youfibre/otsc';
import { ResidentialSwitchOrder } from '@youfibre/otsc';

export default function handleResidentialSwitchMatchConfirmation(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchMatchConfirmation
) {
  console.info(`\n\n✅ Residential Switch Match Confirmed\n\n`);

  // Making an assumption of switching in a week but it could be any future date
  const now = new Date();
  const switchDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 7);
  
  // Find the related Switch Order
  const switchOrder = repositories.switchOrderRepository.findByCorrelationId(body.envelope.destination.correlationID);
  if (!switchOrder) {
    console.error(`\n\n🚨 Could not locate related Switch Order by Correlation ID: ${body.envelope.destination.correlationID}\n\n`);
    return;
  }

  switchOrder.currentStage = 'order';
  switchOrder.switchOrderReference = body.residentialSwitchMatchConfirmation.matchResult.switchOrderReference;
  switchOrder.switchDate = switchDate;

  // @TODO: Implement communication towards supply chain for supply order
  //        Once the supply order has been accepted by the supply chain,
  //        it is then reasonable to place the order, with explicit and
  //        recorded customer consent.

  return client.sendMessage({
    envelope: {
      source: body.envelope.destination,
      destination: body.envelope.source
    },
  
    residentialSwitchOrder: {
      switchOrderReference: switchOrder.switchOrderReference,
      plannedSwitchDate: switchDate.toISOString().split('T')[0]
    }
  } as ResidentialSwitchOrder);
}