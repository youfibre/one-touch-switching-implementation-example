import { RepositoryBundle } from '../../data/repository-bundle.type';
import { OTSClient } from '@youfibre/otsc';
import { ResidentialSwitchMatchFailure } from '@youfibre/otsc';

export default function handleResidentialSwitchMatchFailure(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchMatchFailure
) {
  console.log('\n\n🚨 Received residentialSwitchMatchFailure: manual intervention required.');
  console.log(`🚨 Fault Code: ${body.residentialSwitchMatchFailure.faultCode}`);
  console.log(`🚨 Fault Reason: ${body.residentialSwitchMatchFailure.faultText}`);
  console.log(`🚨 Fault Element: ${body.residentialSwitchMatchFailure.faultElement || 'N/A'}`);
  console.log(`🚨 Fault Element Value: ${body.residentialSwitchMatchFailure.faultElementValue || 'N/A'}\n\n`);

  // @TODO: Implement a way to ask the customer for other information here
  const switchOrder = repositories.switchOrderRepository.findByCorrelationId(body.envelope.destination.correlationID);
  if (switchOrder) {
    if (!switchOrder.switchRequest) return;
    const affectedServiceIds = switchOrder.switchRequest?.affectedServiceIds;
    switchOrder.customer.services = switchOrder.customer.services.filter((s) => !affectedServiceIds.includes(s.serviceId));
    if (switchOrder.customer.services.length === 0) {
      repositories.customerRepository.remove(switchOrder.customer.accountNumber);
    }
  }
}