import { ResidentialSwitchMatchRequest } from '@youfibre/otsc';
import { ResidentialSwitchMatchConfirmation } from '@youfibre/otsc';
import { ResidentialSwitchMatchFailure } from '@youfibre/otsc';
import { OTSClient } from '@youfibre/otsc';
import { RepositoryBundle } from '../../data/repository-bundle.type';
import { VoiceService } from '../../data/customer-repository.class';
import { SwitchActionsArray, SwitchOrder } from '../../data/switch-order-repository.class';
import { v4 } from "uuid";

async function missingOrIncompleteAddress(client: OTSClient, rcpId: string, body: ResidentialSwitchMatchRequest) {
  return await client.sendMessage({
    envelope: {
      source: {
        type: 'RCPID',
        identity: rcpId,
        correlationID: body.envelope.destination.correlationID
      },
      destination: body.envelope.source
    },
  
    residentialSwitchMatchFailure: {
      faultCode: '102',
      faultText: 'Missing, or incomplete address',
      faultElement: 'uprn'
    }
  } as ResidentialSwitchMatchFailure);
}

async function addressNotFound(client: OTSClient, rcpId: string, body: ResidentialSwitchMatchRequest) {
  return client.sendMessage({
    envelope: {
      source: {
        type: 'RCPID',
        identity: rcpId,
        correlationID: body.envelope.destination.correlationID
      },
      destination: body.envelope.source
    },
  
    residentialSwitchMatchFailure: {
      faultCode: '101',
      faultText: 'Address not found',
      faultElement: 'uprn',
      faultElementValue: (body.residentialSwitchMatchRequest?.uprn || '').toString()
    }
  } as ResidentialSwitchMatchFailure);
}

async function accountNotRecognised(
  client: OTSClient,
  rcpId: string,
  body: ResidentialSwitchMatchRequest,
  extra?: any
) {
  return client.sendMessage({
    envelope: {
      source: {
        type: 'RCPID',
        identity: rcpId,
        correlationID: body.envelope.destination.correlationID
      },
      destination: body.envelope.source
    },
  
    residentialSwitchMatchFailure: {
      faultCode: '106',
      faultText: 'Account not recognised',
      ...extra
    }
  } as ResidentialSwitchMatchFailure);
}

async function switchRequestConfirmation(
  client: OTSClient,
  rcpId: string,
  body: ResidentialSwitchMatchRequest,
  switchOrder: SwitchOrder,
  switchActions: SwitchActionsArray
) {
  const customer = switchOrder.customer;
  
  return await client.sendMessage({
    envelope: {
      source: {
        type: 'RCPID',
        identity: rcpId,
        correlationID: switchOrder.correlationId
      },
      destination: body.envelope.source
    },
  
    residentialSwitchMatchConfirmation: {
      implicationsSent: [
        {
          sentMethod: 'email',
          sentTo: 'redacted***@' + customer.email.split('@')[1],
          sentBy: new Date().toISOString().replace('T', ' ').split('.')[0]
        }
      ],
  
      matchResult: {
        switchOrderReference: switchOrder.switchOrderReference,
        matchType: 'consumer',

        services: switchActions.map((switchAction) => {
          const requestedService = switchAction.requestedService;
          const switchActionData = switchAction.switchActionData;
          if (!switchActionData) {
            throw new Error(`SwitchActionData for ${JSON.stringify(requestedService)} is not present.`);
          }

          return {
            serviceType: requestedService.serviceType,
            switchAction: switchActionData?.switchAction,
            serviceIdentifers: (!switchActionData.service || requestedService.serviceType === 'IAS') ? [] : [
              {
                identifierType: 'CUPID',
                identifier: '123'
              },
              {
                identifierType: 'DN',
                identifier: (switchActionData?.service as VoiceService).phoneNumber
              }
            ]
          };
        })
      }
    }
  } as ResidentialSwitchMatchConfirmation);
}

function getLastName(fullName: string) {
  const split = fullName.split(' ');
  return split[split.length-1];
}

export default function handleResidentialSwitchMatchRequest(
  client: OTSClient,
  rcpId: string,
  repositories: RepositoryBundle,
  body: ResidentialSwitchMatchRequest
) {
  if (!body.residentialSwitchMatchRequest.uprn) {
    return missingOrIncompleteAddress(client, rcpId, body);
  }

  // Find the customer
  const customers = repositories.customerRepository.findByUprn(body.residentialSwitchMatchRequest.uprn);
  if (customers.length === 0) {
    return addressNotFound(client, rcpId, body);
  }

  // Get all services at the requested UPRN
  const servicesAtUprn = customers.flatMap((c) => c.services);
  console.log('services found', JSON.stringify(servicesAtUprn, null, 4));

  // Find the matching customer
  const customerByName = customers.find(
    (c) => getLastName(c.name.trim().toLowerCase()) === getLastName(body.residentialSwitchMatchRequest.name.trim().toLowerCase())
  );
  if (!customerByName) {
    return accountNotRecognised(client, rcpId, body, {
      faultElement: 'name',
      faultElementValue: getLastName(body.residentialSwitchMatchRequest.name)
    });
  }

  // Loop through all the requested services and determine switchAction.
  // @GUIDANCE: In a more in-depth implementation, you would likely also determine additional
  // combinations for cease/retain here outside of the scope of the requested services.
  const switchActions: SwitchActionsArray = [];
  for (const service of body.residentialSwitchMatchRequest.services) {
    const matchingServicesBasedOnType = servicesAtUprn.filter((s) => s.type === service.serviceType);
    if (matchingServicesBasedOnType.length === 0) {
      switchActions.push({ requestedService: service, switchActionData: { switchAction: 'ServiceNotFound' } });
      continue;
    }

    for (const matchingService of matchingServicesBasedOnType) {
      // Don't allow matching of the same requestedService : CP Service twice.
      if (Array.from(switchActions.values()).some((sa) => sa.switchActionData.service === matchingService)) {
        continue;
      }

      if (customerByName.services.map((s) => s.serviceId).includes(matchingService.serviceId)) {
        switchActions.push({ requestedService: service, switchActionData: { switchAction: 'ServiceFound', service: matchingService } });
        break;
      }
      // @TODO: Add logic for ForcedCease, OptionToCease, OptionToRetain
    }

    if (!switchActions.find((s) => s.requestedService === service)) {
      switchActions.push({ requestedService: service, switchActionData: { switchAction: 'ServiceNotFound' } });
    }
  }

  // Ensure that there is at least one ServiceFound
  if (!switchActions.some((sa) => sa.switchActionData.switchAction === 'ServiceFound')) {
    return accountNotRecognised(client, rcpId, body);
  }

  // @TODO: Implement notification for switch already in progress
  // @BLOCKED: Cannot proceed with this until we understand the format for the message towards the GP
  // informing that the service is already being switched.
  for (const switchAction of switchActions) {
    if (!switchAction.switchActionData.service) continue;
    if (repositories.switchOrderRepository.hasActiveSwitchOrder(switchAction.switchActionData.service)) {
      console.log(`Active Switch Order found for service ${switchAction.switchActionData.service?.serviceId} but notification format unknown to send to GP.`);
      return accountNotRecognised(client, rcpId, body, {
        faultElement: 'confusion',
        faultElementValue: 'Unsure of the format to inform GP that there is already an active Switch Order.'
      });
    }
  }

  // Create a new Correlation ID and Switch Order Reference
  const switchOrder: SwitchOrder = {
    currentStage: 'request',
    switchOrderReference: v4(),
    correlationId: v4(),
    customer: customerByName,
    switchActions: switchActions,
    losingProviderRcpId: body.envelope.destination.identity,
    gainingProviderRcpId: body.envelope.source.identity
  };
  repositories.switchOrderRepository.add(switchOrder);

  // Send an email to the customer informing them of the implications
  console.log(`\n\n📧 SEND EMAIL TO CUSTOMER: ${customerByName.name} <${customerByName.email}>\nEmail informing customer of implications.\n\n`);

  // Respond to the GP
  return switchRequestConfirmation(client, rcpId, body, switchOrder, switchActions);
}