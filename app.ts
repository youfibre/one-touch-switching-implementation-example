import { MessageType, OTSClient, ResidentialSwitchMatchRequest, ResidentialSwitchOrder, ResidentialSwitchOrderCancellation, ResidentialSwitchOrderCancellationConfirmation, ResidentialSwitchOrderCancellationFailure, ResidentialSwitchOrderConfirmation, ResidentialSwitchOrderFailure, ResidentialSwitchOrderTrigger, ResidentialSwitchOrderTriggerConfirmation, ResidentialSwitchOrderTriggerFailure, ResidentialSwitchOrderUpdate, ResidentialSwitchOrderUpdateConfirmation, ResidentialSwitchOrderUpdateFailure, ResidentialSwitchMatchConfirmation, ResidentialSwitchMatchFailure } from '@youfibre/otsc';
import { Express, Request, Response } from 'express';
import { CustomerRepository } from "./data/customer-repository.class";
import { SwitchOrderRepository } from "./data/switch-order-repository.class";
import handleResidentialSwitchMatchRequest from "./message-handlers/2.1/residential-switch-match-request.handler";
import { RepositoryBundle } from "./data/repository-bundle.type";
import handleResidentialSwitchRequestConfirmation from "./message-handlers/2.1/residential-switch-match-confirmation.handler";
import handleResidentialSwitchOrder from "./message-handlers/2.2/residential-switch-order.handler";
import handleResidentialSwitchRequestFailure from "./message-handlers/2.1/residential-switch-match-failure.handler";
import handleResidentialSwitchOrderConfirmation from "./message-handlers/2.2/residential-switch-order-confirmation.handler";
import handleResidentialSwitchOrderFailure from "./message-handlers/2.2/residential-switch-order-failure.handler";
import handleResidentialSwitchOrderUpdate from "./message-handlers/2.3/residential-switch-order-update.handler";
import handleResidentialSwitchOrderUpdateConfirmation from "./message-handlers/2.3/residential-switch-order-update-confirmation.handler";
import handleResidentialSwitchOrderTrigger from "./message-handlers/2.4/residential-switch-order-trigger.handler";
import handleResidentialSwitchOrderTriggerConfirmation from "./message-handlers/2.4/residential-switch-order-trigger-confirmation.handler";
import handleResidentialSwitchOrderTriggerFailure from "./message-handlers/2.4/residential-switch-order-trigger-failure.handler";
import handleResidentialSwitchOrderCancellation from "./message-handlers/2.5/residential-switch-order-cancellation.handler";
import handleResidentialSwitchOrderCancellationConfirmation from "./message-handlers/2.5/residential-switch-order-cancellation-confirmation.handler";
import handleResidentialSwitchOrderCancellationFailure from "./message-handlers/2.5/residential-switch-order-cancellation-failure.handler";
import handleResidentialSwitchOrderUpdateFailure from "./message-handlers/2.3/residential-switch-order-update-failure.handler";
import { AddressRepository } from "./data/address-repository.class";
import * as express from 'express';
import { handleFormSubmission } from './request-handlers/form-submission.handler';

export async function run(
  port: number,
  rcpId: string,
  hubUsername: string,
  hubPassword: string
) {
  const app: Express = express();
  app.use(express.json());

  const repositories: RepositoryBundle = {
    customerRepository: new CustomerRepository(rcpId),
    switchOrderRepository: new SwitchOrderRepository(),
    addressRepository: new AddressRepository()
  };

  const client = new OTSClient(
    hubUsername,
    hubPassword,
    'amqps://b-d8243155-f1d6-49a4-a135-ce49d17483f5.mq.eu-west-2.amazonaws.com:5671',
    `${rcpId}_otsc.log`,
    `${rcpId}_publicKey.pem`,
    `${rcpId}_privateKey.pem`
  );

  try {
    client.generateKeyPair();
  } catch {
    // It's okay, probably just already exists.
  }

  console.log('Operating with RCPID', rcpId, `(${(await client.getDirectory()).rcpIdDirectory[rcpId].name})`);

  // Register Event Handlers
  // 2.1 Requests
  await client.registerEventHandler(MessageType.GP_RESIDENTIAL_SWITCH_MATCH_REQUEST, (switchMessage: ResidentialSwitchMatchRequest) => {
    handleResidentialSwitchMatchRequest(client, rcpId, repositories, switchMessage);
  });

  await client.registerEventHandler(MessageType.LP_RESIDENTIAL_SWITCH_MATCH_CONFIRMATION, (switchMessage: ResidentialSwitchMatchConfirmation) => {
    handleResidentialSwitchRequestConfirmation(client, rcpId, repositories, switchMessage);
  });

  await client.registerEventHandler(MessageType.LP_RESIDENTIAL_SWITCH_MATCH_FAILURE, (switchMessage: ResidentialSwitchMatchFailure) => {
    handleResidentialSwitchRequestFailure(client, rcpId, repositories, switchMessage);
  });

  // 2.2 Requests
  await client.registerEventHandler(MessageType.GP_RESIDENTIAL_SWITCH_ORDER, (switchMessage: ResidentialSwitchOrder) => {
    handleResidentialSwitchOrder(client, rcpId, repositories, switchMessage);
  });

  await client.registerEventHandler(MessageType.LP_RESIDENTIAL_SWITCH_ORDER_CONFIRMATION, (switchMessage: ResidentialSwitchOrderConfirmation) => {
    handleResidentialSwitchOrderConfirmation(client, rcpId, repositories, switchMessage);
  });

  await client.registerEventHandler(MessageType.LP_RESIDENTIAL_SWITCH_ORDER_FAILURE, (switchMessage: ResidentialSwitchOrderFailure) => {
    handleResidentialSwitchOrderFailure(client, rcpId, repositories, switchMessage);
  });

  // 2.3 Requests
  await client.registerEventHandler(MessageType.GP_RESIDENTIAL_SWITCH_ORDER_UPDATE, (switchMessage: ResidentialSwitchOrderUpdate) => {
    handleResidentialSwitchOrderUpdate(client, rcpId, repositories, switchMessage);
  });

  await client.registerEventHandler(MessageType.LP_RESIDENTIAL_SWITCH_ORDER_UPDATE_CONFIRMATION, (switchMessage: ResidentialSwitchOrderUpdateConfirmation) => {
    handleResidentialSwitchOrderUpdateConfirmation(client, rcpId, repositories, switchMessage);
  });

  await client.registerEventHandler(MessageType.LP_RESIDENTIAL_SWITCH_ORDER_UPDATE_FAILURE, (switchMessage: ResidentialSwitchOrderUpdateFailure) => {
    handleResidentialSwitchOrderUpdateFailure(client, rcpId, repositories, switchMessage);
  });

  // 2.4 Requests
  await client.registerEventHandler(MessageType.GP_RESIDENTIAL_SWITCH_ORDER_TRIGGER, (switchMessage: ResidentialSwitchOrderTrigger) => {
    handleResidentialSwitchOrderTrigger(client, rcpId, repositories, switchMessage);
  });

  await client.registerEventHandler(MessageType.LP_RESIDENTIAL_SWITCH_ORDER_TRIGGER_CONFIRMATION, (switchMessage: ResidentialSwitchOrderTriggerConfirmation) => {
    handleResidentialSwitchOrderTriggerConfirmation(client, rcpId, repositories, switchMessage);
  });

  await client.registerEventHandler(MessageType.LP_RESIDENTIAL_SWITCH_ORDER_TRIGGER_FAILURE, (switchMessage: ResidentialSwitchOrderTriggerFailure) => {
    handleResidentialSwitchOrderTriggerFailure(client, rcpId, repositories, switchMessage);
  });

  // 2.5 Requests
  await client.registerEventHandler(MessageType.GP_RESIDENTIAL_SWITCH_ORDER_CANCELLATION, (switchMessage: ResidentialSwitchOrderCancellation) => {
    handleResidentialSwitchOrderCancellation(client, rcpId, repositories, switchMessage);
  });

  await client.registerEventHandler(MessageType.LP_RESIDENTIAL_SWITCH_ORDER_CANCELLATION_CONFIRMATION, (switchMessage: ResidentialSwitchOrderCancellationConfirmation) => {
    handleResidentialSwitchOrderCancellationConfirmation(client, rcpId, repositories, switchMessage);
  });

  await client.registerEventHandler(MessageType.LP_RESIDENTIAL_SWITCH_ORDER_CANCELLATION_FAILURE, (switchMessage: ResidentialSwitchOrderCancellationFailure) => {
    handleResidentialSwitchOrderCancellationFailure(client, rcpId, repositories, switchMessage);
  });

  // Fetch the name of the current CP from the directory for use in the form
  const rcpName = (await client.getDirectory())?.rcpIdDirectory[rcpId].name;

  // index.html can be used to display a basic customer flow
  app.get('/', (req: Request, res: Response) => {
    res.sendFile(__dirname + '/index.html');
  });

  // status.html can be used to see the current state of customers, their services,
  // and switch orders on the system
  app.get('/status', (req: Request, res: Response) => {
    res.sendFile(__dirname + '/status.html');
  });

  // This is used to inform index.html of the available RCPs
  app.get('/dir', async (req: Request, res: Response) => {
    res.send(await client.getDirectory());
  });

  // This is used to inform index.html of the current rcpId/rcpName,
  // and to inform status.html of all current details within the system
  app.get('/app-status', async (req: Request, res: Response) => {
    res.send({
      rcpId,
      rcpName,
      customers: repositories.customerRepository.asArray(),
      switchOrders: repositories.switchOrderRepository.asArray(),
      addresses: repositories.addressRepository.asArray()
    });
  });

  // This handles the form submission for index.html and kicks off the
  // residentialSwitchRequest
  app.post('/form-submission', handleFormSubmission.bind({
    rcpId,
    rcpName,
    repositories,
    client
  }));

  // Expose an endpoint to get the latest report
  app.get('/report', (req: Request, res: Response) => {
    res.send(client.generateReport());
  })

  app.listen(port, () => {
    console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
  });
}