import { Address } from '@youfibre/otsc';

export interface Service {
  serviceId: string;
}

export interface VoiceService extends Service {
  type: 'NBICS';
  phoneNumber: string;
}

export interface BroadbandService extends Service {
  type: 'IAS'
}

export interface Customer {
  accountNumber: string;
  name: string;
  email: string;
  uprn: number;
  address: Address;
  services: (BroadbandService | VoiceService)[];
}

export class CustomerRepository {
  private customers: Map<string, Customer>;

  constructor(rcpId: string) {
    this.customers = new Map<string, Customer>([]);

    if (rcpId === 'RCP004') {
      // Add some dummy accounts
      this.customers.set('ACC001', {
        accountNumber: 'ACC001',
        name: 'John Doe',
        email: `john.doe@example.com`,
        address: {
          addressLines: ['Dummy Address', 'Rose Cottage', '22 Cheshunt Mews', 'Cypress Street', 'Tyre Industrial Estate', 'Blnatyre'],
          postTown: 'Glasgow',
          postCode: 'SW1P 3UX'
        },
        uprn: 123,
        services: [{
          serviceId: 'SVC001',
          type: 'IAS'
        }],
        // activeSwitchRequests: []
      });

      this.customers.set('ACC002', {
        accountNumber: 'ACC002',
        name: 'Jane Doe',
        email: `jane.doe@example.com`,
        address: {
          addressLines: ['Dummy Address', 'Rose Cottage', '22 Cheshunt Mews', 'Cypress Street', 'Tyre Industrial Estate', 'Blnatyre'],
          postTown: 'Glasgow',
          postCode: 'SW1P 3UX'
        },
        uprn: 124,
        services: [{
          serviceId: 'SVC002',
          type: 'IAS'
        }, {
          serviceId: 'SVC003',
          type: 'NBICS',
          phoneNumber: '01443111222'
        }],
        // activeSwitchRequests: []
      });

      this.customers.set('ACC003', {
        accountNumber: 'ACC003',
        name: 'John Moor',
        email: `john.moor@example.com`,
        address: {
          addressLines: ['Dummy Address', 'Rose Cottage', '22 Cheshunt Mews', 'Cypress Street', 'Tyre Industrial Estate', 'Blnatyre'],
          postTown: 'Glasgow',
          postCode: 'SW1P 3UX'
        },
        uprn: 125,
        services: [{
          serviceId: 'SVC004',
          type: 'IAS'
        }],
        // activeSwitchRequests: []
      });
    }
  }

  public findByUprn(uprn: number): Customer[] {
    // Definitely not efficient, avoid in production. :-)
    return Array.from(this.customers.values()).filter((c) => c.uprn === uprn);
  }

  public get(id: string): Customer | undefined {
    return this.customers.get(id);
  }

  public add(customer: Customer): void {
    this.customers.set(customer.accountNumber, customer);
  }

  public remove(accountNumber: string): void {
    this.customers.delete(accountNumber);
  }

  public count(): number {
    return this.customers.size;
  }

  public asArray(): Customer[] {
    return Array.from(this.customers.values());
  }
}