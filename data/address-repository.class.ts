import { Address } from '@youfibre/otsc';

// The purpose of this is to act like a source of address data (like PAF)
export class AddressRepository {
  private addresses: Map<number, Address>;

  constructor() {
    this.addresses = new Map<number, Address>();

    this.addresses.set(
      123,
      {
        addressLines: ['Dummy Address 1', 'Rose Cottage', '22 Cheshunt Mews', 'Cypress Street', 'Tyre Industrial Estate', 'Blnatyre'],
        postTown: 'Glasgow',
        postCode: 'SW1P 3UX'
      }
    );

    this.addresses.set(
      124,
      {
        addressLines: ['Dummy Address 2', 'Rose Cottage', '22 Cheshunt Mews', 'Cypress Street', 'Tyre Industrial Estate', 'Blnatyre'],
        postTown: 'Glasgow',
        postCode: 'SW1P 3UX'
      }
    );

    this.addresses.set(
      125,
      {
        addressLines: ['Dummy Address 3', 'Rose Cottage', '22 Cheshunt Mews', 'Cypress Street', 'Tyre Industrial Estate', 'Blnatyre'],
        postTown: 'Glasgow',
        postCode: 'SW1P 3UX'
      }
    );
  }

  public findByUprn(uprn: number): Address | undefined {
    return this.addresses.get(uprn);
  }

  public count(): number {
    return this.addresses.size;
  }

  public asArray(): { uprn: number, address: Address }[] {
    return Array.from(this.addresses.keys()).reduce((arr, uprn) => {
      const address = this.addresses.get(uprn);
      if (address) {
        arr.push({
          uprn,
          address
        });
      }
      return arr;
    }, [] as { uprn: number, address: Address }[]) || [];
  }
}