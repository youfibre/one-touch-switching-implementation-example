import { ResidentialSwitchMatchRequest } from '@youfibre/otsc';
import { BroadbandService, Customer, VoiceService } from './customer-repository.class';

export type RequestedService = {
  serviceType: 'IAS';
  action: 'cease';
} | {
  serviceType: 'NBICS';
  serviceIdentifierType?: 'DN';
  serviceIdentifier?: string;
  action: 'cease' | 'port'
};

export type SwitchActionData = {
  switchAction: string,
  service?: BroadbandService | VoiceService
};

export type SwitchActionsArray = {
  requestedService: RequestedService,
  switchActionData: SwitchActionData
}[];

export interface SwitchOrder {
  currentStage: 'request' | 'order' | 'completed' | 'cancelled';
  switchOrderReference: string;
  correlationId: string;
  customer: Customer;
  switchActions?: SwitchActionsArray; // Used only if losing a customer
  switchRequest?: { request: ResidentialSwitchMatchRequest, affectedServiceIds: string[] }; // Used only if gaining a customer
  losingProviderRcpId: string;
  gainingProviderRcpId: string;
  switchDate?: Date;
}

export class SwitchOrderRepository {
  private switchOrders: SwitchOrder[];

  constructor() {
    this.switchOrders = [];
  }

  public add(switchOrder: SwitchOrder): void {
    this.switchOrders.push(switchOrder);
  }

  public count(): number {
    return this.switchOrders.length;
  }

  public findByOrderReference(orderReference: string): SwitchOrder | undefined {
    return this.switchOrders.find((so) => so.switchOrderReference === orderReference);
  }

  public findByCorrelationId(correlationId: string): SwitchOrder | undefined {
    return this.switchOrders.find((so) => so.correlationId === correlationId);
  }

  public hasActiveSwitchOrder(service: BroadbandService | VoiceService): boolean {
    for (const switchOrder of this.switchOrders) {
      if (switchOrder.currentStage !== 'order') continue;
      for (const switchAction of switchOrder.switchActions || []) {
        if (switchAction.switchActionData.service === service) {
          return true;
        }
      }

      for (const affectedServiceId of switchOrder.switchRequest?.affectedServiceIds || []) {
        if (affectedServiceId === service.serviceId) {
          return true;
        }
      }
    }
    return false;
  }

  public asArray(): SwitchOrder[] {
    return this.switchOrders;
  }
}