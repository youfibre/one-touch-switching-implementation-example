import { AddressRepository } from "./address-repository.class";
import { CustomerRepository } from "./customer-repository.class";
import { SwitchOrderRepository } from "./switch-order-repository.class";

export type RepositoryBundle = {
  customerRepository: CustomerRepository;
  switchOrderRepository: SwitchOrderRepository;
  addressRepository: AddressRepository;
};