import { ResidentialSwitchOrderUpdateFailure } from '@youfibre/otsc';

export default {
  envelope: {
    source: {
      type: 'RCPID',
      identity: 'ABCD',
      correlationID: 'XYZ987'
    },
    destination: {
      type: 'RCPID',
      identity: 'ABCD',
      correlationID: 'XYZ123'
    }
  },

  residentialSwitchOrderUpdateFailure: {
    faultCode: '301',
    faultText: 'Invalid or missing switch order reference',
    faultElement: 'switchOrderReference',
    faultElementValue: ''
  }
} as ResidentialSwitchOrderUpdateFailure;