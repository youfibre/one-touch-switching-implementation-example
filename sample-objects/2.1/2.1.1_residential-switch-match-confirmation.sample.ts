import { ResidentialSwitchMatchConfirmation } from '@youfibre/otsc';

export default {
  envelope: {
    source: {
      type: 'RCPID',
      identity: 'ABCD',
      correlationID: 'XYZ987'
    },
    destination: {
      type: 'RCPID',
      identity: 'ABCD',
      correlationID: 'XYZ123'
    }
  },

  residentialSwitchMatchConfirmation: {
    implicationsSent: [
      {
        sentMethod: 'email',
        sentTo: '****redacted@example.com',
        sentBy: '2023-01-31 00:00:00'
      }
    ],

    matchResult: {
      switchOrderReference: '123e4567-e89b-12d3-a456-426614174000',
      matchType: 'consumer',
      services: [
        {
          serviceType: 'IAS',
          switchAction: 'ServiceFound',
          serviceIdentifiers: [
            {
              identifierType: 'ONTReference',
              identifier: '123456789'
            },
            {
              identifierType: 'ONTPortNumber',
              identifier: '1'
            },
            {
              identifierType: 'NetworkOperator',
              identifier: 'Openreach'
            }
          ]
        }
      ]
    },

    alternativeSwitchOrders: [
      {
        matchResult: {
          switchOrderReference: '123e4567-e89b-12d3-a456-426614174001',
          services: [
            {
              serviceType: "IAS",
              switchAction: "ServiceFound",
              ACPID: "Openreach",
              serviceIdentifers: [
                {
                  identifierType: "ONTReference",
                  identifier: "123456789"
                },
                {
                  identifierType: "ONTPortNumber",
                  identifier: "1"
                },
                {
                  identifierType: "NetworkOperator",
                  identifier: "Openreach"
                }
              ]
            },
            {
              serviceType: 'NBICS',
              switchAction: 'ServiceFound',
              serviceIdentifers: [
                {
                  identifierType: 'CUPID',
                  identifier: '123'
                },
                {
                  identifierType: 'DN',
                  identifier: '0101111222'
                }
              ]
            },
            {
              serviceType: 'NBICS',
              switchAction: 'OptionToCease',
              serviceIdentifiers: [
                {
                  identifierType: 'PartialDN',
                  identifier: '13'
                }
              ]
            }
          ]
        }
      }
    ]
  }
} as ResidentialSwitchMatchConfirmation;