import { run } from "./app";
import * as dotenv from 'dotenv';

dotenv.config();

// localhost:8001 will run RCP001 - YouFibre
run(
  8001,
  'RCP001',
  'rcp001_youfibre',
  process.env.RCP001_HUB_PASSWORD
);