import { run } from "./app";
import * as dotenv from 'dotenv';

dotenv.config();

// localhost:8004 will run RCP004 - Brsk
run(
  8004,
  'RCP004',
  'rcp004_brsk',
  process.env.RCP004_HUB_PASSWORD
);